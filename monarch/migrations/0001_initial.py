# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='RecordLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('legacy_table', models.CharField(max_length=b'100')),
                ('legacy_pk_field', models.CharField(max_length=b'100')),
                ('legacy_pk_value', models.CharField(max_length=b'100')),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='recordlink',
            unique_together=set([('legacy_table', 'legacy_pk_field', 'legacy_pk_value')]),
        ),
    ]
