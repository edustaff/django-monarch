__author__ = 'kclark'

from distutils.core import setup
from setuptools import setup, find_packages


setup(
    name='django-monarch',
    version='0.1.0',
    author='EDUStaff',
    author_email='it@edustaff.org',
    description='Stuff.',
    packages=find_packages(exclude=['example', 'example.*', 'docs', 'dist']),
    include_package_data=True,
    url='www.edustaff.org',
)